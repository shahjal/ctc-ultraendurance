#### Ultra Endurance training program from The Time-Crunched Cyclist
Files are in MRC format. MRC is text based format which encodes FTP in %, not static values. It is supported by most training apps. You can possibly use this in [Flux](https://github.com/dvmarinoff/Flux) if you don't want to use any commercial app, either by adding it to the [workouts](https://github.com/dvmarinoff/Flux/blob/master/src/workouts/workouts.js) or using Zwift [Converter](https://whatsonzwift.com/convert/workouts) to convert it to fit file for importing to Flux.

